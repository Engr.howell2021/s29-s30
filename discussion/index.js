// Create an Api Server using Express

// Identify the proper ingredients/material/ componets needed to start the project.

// use the 'require directive to load the xpress module/package.
// express => this will allow us to access methods and fuctions that will creating a server easier

const express = require('express')

// create an application using express.
// express() -> this creates an express/instant application and we will give an identifier for the app that will proudce.

const application = express();

// identify a cirtual port in which to serve the project server
const port = 4000;
// assign the established connection/server into the designated port.
application.listen(port, () => console.log(`server is running ${port}`));


